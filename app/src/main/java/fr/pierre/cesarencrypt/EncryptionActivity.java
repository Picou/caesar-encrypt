package fr.pierre.cesarencrypt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.pierre.cesarencrypt.Core.Caesar;
import fr.pierre.cesarencrypt.Core.CaesarClassic;
import fr.pierre.cesarencrypt.Core.IEncryptionMethod;

public class EncryptionActivity extends AppCompatActivity {
    private boolean encrypting = true;
    private IEncryptionMethod method;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encryption);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            switch(bundle.getInt("position")) {
                default:
                case 0:
                    method = new Caesar();
                    break;
                case 1:
                    method = new CaesarClassic();
                    break;
            }
        }
    }

    public void onClickBtn(View v) {
        if (encrypting) {
            ((Button) findViewById(R.id.btnEncrypt)).setText("Déchiffrer");
            if(method != null){
                String toEncrypt = ((EditText) findViewById(R.id.txtToEncrypt)).getText().toString();
                ((EditText) findViewById(R.id.txtToEncrypt)).setText(method.encrypt(toEncrypt));
            }
        } else {
            ((Button) findViewById(R.id.btnEncrypt)).setText("Chiffrer");
            if(method != null){
                String toDecrypt = ((EditText) findViewById(R.id.txtToEncrypt)).getText().toString();
                ((EditText) findViewById(R.id.txtToEncrypt)).setText(method.decrypt(toDecrypt));
            }
        }
        this.encrypting = !this.encrypting;
    }
}
