package fr.pierre.cesarencrypt.Core;

public interface IEncryptionMethod {
    public String encrypt(String sIn);
    public String decrypt(String toCrypt);
}
