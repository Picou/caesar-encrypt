package fr.pierre.cesarencrypt.Core;

public class Caesar implements IEncryptionMethod {
    private static char[] alphabet1 =
            {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',  ',', '.', '-'};

    //on utilise une chaine de caractère de 500 plutot que de recalculer pi
    private static String pi = "31415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421" +
            "1706798214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196" +
            "4428810975665933446128475648233786783165271201909145648566923460348610454326648213393607260249141273724587" +
            "0066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094330572703657" +
            "5959195309218611738193261179310511854807446237996274956735188575272489122793818301194912";

    public String encrypt(String sIn) {
        /*
         * On convertit le string en un tableau de char
         * On crée un tableau Out de même taille
         * On prend chaque lettre du string
         * On prend sa position actuelle dans l'alphabet
         * On va chercher dans l'alphabet la lettre correspondant à la nouvelle position
         * On assigne cette lettre dans le tableau de sortie au mm emplacement
         * On transforme le tableau Out en un string
         */

        char[] charSIn = sIn.toCharArray();
        char[] charSOut = new char[charSIn.length];
        int pos1, pos2;
        for (int i = 0; i < charSIn.length; i++) {
            pos1 = posChar(charSIn[i], alphabet1);

            if (pos1 <= -1)
                charSOut[i] = charSIn[i]; // si -1, c'est que ce n'est pas une lettre, on met la lettre de base
            else {
                pos2 = newPos(pos1, i, true);
                charSOut[i] = alphabet1[pos2];
            }
        }
        return new String(charSOut); // on fait un string avec le tableau de char
    }

    public String decrypt(String sIn) {
        /*
         * La marche a suivre est la meme que pour le crypt sauf qu'on prend un n' = -n;
         */

        char[] charSIn = sIn.toCharArray();
        char[] charSOut = new char[charSIn.length];
        int pos1, pos2;
        for (int i = 0; i < charSIn.length; i++) {
            pos1 = posChar(charSIn[i], alphabet1);
            if (pos1 <= -1)
                charSOut[i] = charSIn[i]; // si -1, c'est que ce n'est pas une lettre, on met la lettre de base
            else {
                pos2 = newPos(pos1, i, false);
                charSOut[i] = alphabet1[pos2];
            }
        }
        return new String(charSOut); // on fait un string avec le tableau de char
    }

    // Renvoie la position du caractere dans le tableau
    // -1 si il n'est pas dans le tableau

    private static int posChar(char c, char[] tab) {
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == c) return i;
        }
        return -1;
    }

    private static int newPos(int pos, int offset, boolean encrypting) {
        if (encrypting) {
            pos += Character.getNumericValue(pi.charAt(offset));
            pos = pos % alphabet1.length;
            return pos;
        } else {
            pos -= Character.getNumericValue(pi.charAt(offset));
            pos = abs(pos) % alphabet1.length;
            return pos;
        }
    }
    // Donne la nouvelle position dans l'alphabet en fonction de l'index du caracère
    /*private static int newPos(int pos, int offset, boolean encrypting)  {
        if(pos >= 0) {
            if(encrypting)
                pos += pi.charAt(offset);
            else
                pos -= pi.charAt(offset);
            pos = abs(pos) % alphabet1.length;
        }
        return pos;
    }*/

    // Valeur absolue de a
    public static int abs(int a) {
        if (a >= 0) return a;
        else return -a;
    }
}
